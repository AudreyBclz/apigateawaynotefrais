package com.example.notefraisapigateaway.tool;

import org.springframework.core.io.FileSystemResource;
import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

@Service
public class RestClientUpload<T,V> {
    private String server = "http://localhost:8082";
    private RestTemplate template;
    private HttpHeaders headers;
    private HttpStatusCode status;

    public RestClientUpload(){
        template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());

        headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
    }
    public T post(String server, String uri, MultipartFile data, Class<T>type)throws IOException {
        MultiValueMap<String, String> fileMap = new LinkedMultiValueMap<String, String>();
        ContentDisposition contentDisposition = ContentDisposition.builder("form-data")
                .name("file")
                .filename(data.getOriginalFilename())
                .build();
        fileMap.add(HttpHeaders.CONTENT_DISPOSITION,contentDisposition.toString());
        MultiValueMap<String,Object> body = new LinkedMultiValueMap<>();
        HttpEntity<byte[]>fileEntity = new HttpEntity<>(data.getBytes(),fileMap);
        body.add("file",fileEntity);
        HttpEntity<MultiValueMap<String,Object>>requestEntity = new HttpEntity<>(body,headers);

        ResponseEntity<T> responseEntity = template.exchange(server+"/"+uri, HttpMethod.POST,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
}
