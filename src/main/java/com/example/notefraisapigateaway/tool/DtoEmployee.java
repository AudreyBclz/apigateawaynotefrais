package com.example.notefraisapigateaway.tool;

import com.example.librairienotefrais.dto.ResponseEmployeeDto;
import com.example.notefraisapigateaway.entity.Authority;
import com.example.notefraisapigateaway.entity.Employee;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
public class DtoEmployee {
    public Employee convertToEntity( ResponseEmployeeDto dto){
        Employee employee = new Employee();
        employee.setPassword(dto.getPassword());
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setId(dto.getId());
        Authority authority = new Authority();
        if(dto.getAuthorities() ==1){
            authority.setId(1);
            authority.setName("ROLE_ADMIN");
        } else if (dto.getAuthorities()==2) {
            authority.setId(2);
            authority.setName("ROLE_USER");
        }
        Set<Authority> authoritySet = new HashSet<>();
        authoritySet.add(authority);
        employee.setAuthorities(authoritySet);
        return  employee;
    }

    public ResponseEmployeeDto convertToDto(Employee employee){
        return new ResponseEmployeeDto(employee.getId(), employee.getFirstName(), employee.getLastName(), employee.getPassword(), employee.getAuthorities().iterator().next().getId());
    }
}
