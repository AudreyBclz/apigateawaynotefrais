package com.example.notefraisapigateaway.tool;

import org.springframework.http.*;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@Service
public class RestClient<T,V> {

    private List<String> servers = Arrays.asList("http://localhost:8081","http://localhost:8082");
    private RestTemplate template;
    private HttpHeaders headers;
    private HttpStatusCode status;

    public List<String>getServers(){return servers;}

    public RestClient(){
        template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        headers=new HttpHeaders();
        headers.add("Accept","*/*");
        headers.add("content-type","application/json");
    }
    public RestClient(String type){
        template = new RestTemplate(new HttpComponentsClientHttpRequestFactory());
        headers=new HttpHeaders();
        headers.add("Accept","*/*");
        if(type.equals("upload")){
            headers.add("content-type","multipart/form-data");
        }else{
            headers.add("content-type","application/json");
        }

    }

    public T post(String server,String uri,V data,Class<T>type){
        HttpEntity<V> requestEntity = new HttpEntity<>(data,headers);
        ResponseEntity<T> responseEntity = template.exchange(server+"/"+uri, HttpMethod.POST,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T patch(String server,String uri,V data,Class<T> type,int id){
        HttpEntity<V> requestEntity = new HttpEntity<>(data,headers);
        ResponseEntity<T>responseEntity = template.exchange(server+"/"+uri+"/"+id,HttpMethod.PATCH,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }

    public T get(String server,String uri,Class<T> type){
        HttpEntity<String> requestEntity = new HttpEntity<>("",headers);
        ResponseEntity<T> responseEntity = template.exchange(server+"/"+uri,HttpMethod.GET,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T getSomeById (String server, String uri,Class<T> type,int id){
        HttpEntity<String> requestEntity = new HttpEntity<>("",headers);
        ResponseEntity<T>responseEntity = template.exchange(server+"/"+uri+"/"+id,HttpMethod.GET,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
    public T deleteById(String server,String uri,Class<T> type,int id){
        HttpEntity<String> requestEntity = new HttpEntity<>("",headers);
        ResponseEntity<T>responseEntity = template.exchange(server+"/"+uri+"/"+id,HttpMethod.DELETE,requestEntity,type);
        status = responseEntity.getStatusCode();
        return responseEntity.getBody();
    }
}
