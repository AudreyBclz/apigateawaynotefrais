//Partie Spring Security
package com.example.notefraisapigateaway.tool;

import com.example.notefraisapigateaway.constant.SecurityConstant;
import com.example.notefraisapigateaway.entity.UserDetailImpl;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.security.Keys;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.nio.charset.StandardCharsets;
import java.util.Date;
@Service
public class JwtUtils {
    private String jwtSecret;

    public String generateJwtToken(Authentication authentication){
        UserDetailImpl userDetail = (UserDetailImpl) authentication.getPrincipal();
        return Jwts.builder()
                .setSubject(userDetail.getUsername())
                .setIssuedAt(new Date())
                .setExpiration(new Date(new Date().getTime()+3600000))
                .signWith(Keys.hmacShaKeyFor(SecurityConstant.JWT_KEY.getBytes(StandardCharsets.UTF_8))).compact();
    }
    public boolean validateJwtToken(String token){
        try{
            Jwts.parserBuilder().setSigningKey(Keys.hmacShaKeyFor(SecurityConstant.JWT_KEY.getBytes(StandardCharsets.UTF_8)))
                    .build()
                    .parse(token);
            return true;
        }catch (Exception ex){
            return false;
        }
    }

    public String getUsernameFromJwt(String token){
        return String.valueOf(Jwts.parserBuilder().setSigningKey(Keys.hmacShaKeyFor(SecurityConstant.JWT_KEY.getBytes(StandardCharsets.UTF_8)))
                .build().parseClaimsJws(token).getBody().getSubject());
    }
}
