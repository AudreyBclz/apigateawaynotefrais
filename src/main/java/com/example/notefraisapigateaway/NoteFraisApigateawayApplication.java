package com.example.notefraisapigateaway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NoteFraisApigateawayApplication {

    public static void main(String[] args) {
        SpringApplication.run(NoteFraisApigateawayApplication.class, args);
    }

}
