//Login avec spring security
package com.example.notefraisapigateaway.controller;

import com.example.notefraisapigateaway.dto.LoginRequestDto;
import com.example.notefraisapigateaway.dto.LoginResponseDto;
import com.example.notefraisapigateaway.entity.Employee;
import com.example.notefraisapigateaway.service.EmployeeService;
import com.example.notefraisapigateaway.tool.DtoEmployee;
import com.example.notefraisapigateaway.tool.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
@RestController
@CrossOrigin(value = {"http://localhost:4200","http://localhost:8100","http://localhost:8080"},methods = {RequestMethod.GET,RequestMethod.DELETE,RequestMethod.POST})
@EnableMethodSecurity
public class LoginController {

    @Autowired
    private AuthenticationManager authenticationManager;
    @Autowired
    private JwtUtils _jwtUtils;
    @Autowired
    private DtoEmployee _dtoUtils;
    @Autowired
    private EmployeeService _employeeService;
    @PreAuthorize("permitAll()")
    @PostMapping("/login")
    public ResponseEntity<LoginResponseDto> loginUser(@RequestBody LoginRequestDto loginRequestDto)throws Exception{
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(loginRequestDto.getUsername(), loginRequestDto.getPassword()));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            String token = _jwtUtils.generateJwtToken(authentication);
            Employee employee =_dtoUtils.convertToEntity( _employeeService.findBySearch(loginRequestDto.getUsername()));
            return ResponseEntity.ok(LoginResponseDto.builder().token(token).username(loginRequestDto.getUsername()).id(employee.getId()).build());
        }catch (Exception ex){
            throw ex;
        }
    }
}
