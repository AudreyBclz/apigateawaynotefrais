package com.example.notefraisapigateaway.controller;

import com.example.librairienotefrais.dto.RequestCategoryDto;
import com.example.librairienotefrais.dto.ResponseCategoryDto;
import com.example.notefraisapigateaway.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("category")
@CrossOrigin(value = {"http://localhost:4200","http://localhost:8100","http://localhost:8080"},methods = {RequestMethod.GET,RequestMethod.DELETE,RequestMethod.POST})
@EnableMethodSecurity
public class CategoryController {
    @Autowired
    private CategoryService _categoryService;
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("")
    public ResponseEntity<ResponseCategoryDto>create(@RequestBody RequestCategoryDto dto){
        return ResponseEntity.ok(_categoryService.create(dto));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/{id}")
    public ResponseEntity<ResponseCategoryDto>findById(@PathVariable int id){
        return ResponseEntity.ok(_categoryService.findById(id));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<String>delete(@PathVariable int id){
        return ResponseEntity.ok(_categoryService.delete(id));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("")
    public ResponseEntity<ResponseCategoryDto[]>findAll(){
        return ResponseEntity.ok(_categoryService.findAll());
    }
}
