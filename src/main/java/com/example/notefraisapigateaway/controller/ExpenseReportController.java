package com.example.notefraisapigateaway.controller;

import com.example.librairienotefrais.dto.RequestExpenseReportDto;
import com.example.librairienotefrais.dto.ResponseExpenseReportDto;
import com.example.notefraisapigateaway.service.ExpenseReportService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@RestController
@RequestMapping("expense-report")
@CrossOrigin(value = {"http://localhost:4200","http://localhost:8100","http://localhost:8080"},methods = {RequestMethod.GET,RequestMethod.PATCH,RequestMethod.POST})
@EnableMethodSecurity
public class ExpenseReportController {

    @Autowired
    private ExpenseReportService _expenseService;

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PostMapping("")
    public ResponseEntity<ResponseExpenseReportDto>createExpenseReport(@RequestBody RequestExpenseReportDto dto){
        return ResponseEntity.ok(_expenseService.createExpenseReport(dto));
    }

    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @PostMapping(value = "upload/{id}",consumes = {MediaType.MULTIPART_FORM_DATA_VALUE})
    public ResponseEntity<ResponseExpenseReportDto>uploadAndCreateProof(@PathVariable int id, @RequestParam MultipartFile file)throws IOException {
        return ResponseEntity.ok(_expenseService.uploadAndCreationProof(file,id));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("/{id}")
    public ResponseEntity<ResponseExpenseReportDto>findOneById(@PathVariable int id){
        return ResponseEntity.ok(_expenseService.findById(id));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("category/{id}")
    public ResponseEntity<ResponseExpenseReportDto[]> findByCategory(@PathVariable int id){
        return ResponseEntity.ok(_expenseService.findBySomeId(id,"/category"));
    }
    @PreAuthorize("hasAnyRole('ADMIN','USER')")
    @GetMapping("employee/{id}")
    public ResponseEntity<ResponseExpenseReportDto[]> findByEmployee(@PathVariable int id){
        return ResponseEntity.ok(_expenseService.findBySomeId(id,"/employee"));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("admin/{id}")
    public ResponseEntity<ResponseExpenseReportDto[]> findByAdmin(@PathVariable int id){
        return ResponseEntity.ok(_expenseService.findBySomeId(id,"/admin"));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("")
    public ResponseEntity<ResponseExpenseReportDto[]>findAll(){
        return  ResponseEntity.ok(_expenseService.findAll());
    }
    @PreAuthorize("hasRole('ADMIN')")
    @PatchMapping("/{id}")
    public ResponseEntity<ResponseExpenseReportDto>updateStatus(@PathVariable int id, @RequestBody RequestExpenseReportDto dto){
        return ResponseEntity.ok(_expenseService.updateStatus(dto,id));
    }

}
