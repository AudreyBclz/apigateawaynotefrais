package com.example.notefraisapigateaway.controller;

import com.example.librairienotefrais.dto.RequestEmployeeDto;
import com.example.librairienotefrais.dto.ResponseEmployeeDto;
import com.example.notefraisapigateaway.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("employee")
@CrossOrigin(value = {"http://localhost:4200","http://localhost:8100","http://localhost:8080"},methods = {RequestMethod.GET,RequestMethod.DELETE,RequestMethod.POST})
@EnableMethodSecurity
public class EmployeeController {
    @Autowired
    private EmployeeService _employeeService;
    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping("")
    public ResponseEntity<ResponseEmployeeDto>create(@RequestBody RequestEmployeeDto dto){
        return ResponseEntity.ok(_employeeService.create(dto));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("")
    public ResponseEntity<ResponseEmployeeDto[]>findAll(){
        return ResponseEntity.ok(_employeeService.findAll());
    }
    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping("/{id}")
    public ResponseEntity<ResponseEmployeeDto>findById(@PathVariable int id){
        return ResponseEntity.ok(_employeeService.findById(id));
    }
    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseEmployeeDto>delete(@PathVariable int id){
        return ResponseEntity.ok(_employeeService.delete(id));
    }

    @PreAuthorize("permitAll()")
    @GetMapping("/login/{search}")
    public ResponseEntity<ResponseEmployeeDto> findBySearch(@PathVariable String search){
        return ResponseEntity.ok(_employeeService.findBySearch(search));
    }
}
