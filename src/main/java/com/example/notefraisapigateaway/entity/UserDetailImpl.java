//partie pour Spring Security
package com.example.notefraisapigateaway.entity;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;
import java.util.stream.Collectors;

public class UserDetailImpl implements UserDetails {
    private int id;
    private String username;
    private String password;
    private Collection<? extends GrantedAuthority> authorities;

    public UserDetailImpl(int id, String username,String password,Collection<? extends GrantedAuthority> authorities){
        this.id=id;
        this.username=username;
        this.password=password;
        this.authorities = authorities;
    }

    public static UserDetailImpl build(Employee employee){
        Collection<? extends GrantedAuthority>grantedAuthorities =
                employee.getAuthorities().stream().map(authority -> new SimpleGrantedAuthority(authority.getName()))
                        .collect(Collectors.toList());
        return new UserDetailImpl((employee.getId()), employee.getLastName()+"."+employee.getFirstName(), employee.getPassword(), grantedAuthorities);
    }
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setAuthorities(Collection<? extends GrantedAuthority> authorities) {
        this.authorities = authorities;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
