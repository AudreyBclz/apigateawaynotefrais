package com.example.notefraisapigateaway.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.HashSet;
import java.util.Set;

@Data
@NoArgsConstructor

public class Employee {
    private int id;

    private String lastName;
    private String firstName;
    private String password;
    private Set<Authority> authorities;

    public Employee(int id, String lastName, String firstName, String password) {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.password = password;
        this.authorities=new HashSet<>();
    }
}
