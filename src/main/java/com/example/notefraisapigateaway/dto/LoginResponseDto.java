package com.example.notefraisapigateaway.dto;

import com.example.notefraisapigateaway.entity.Authority;
import lombok.Builder;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;
import java.util.Collections;
import java.util.Set;

@Data
@Builder
public class LoginResponseDto {
    private String token;
    private String username;
    private int id;
}
