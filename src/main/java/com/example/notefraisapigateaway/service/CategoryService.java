package com.example.notefraisapigateaway.service;

import com.example.librairienotefrais.dto.RequestCategoryDto;
import com.example.librairienotefrais.dto.ResponseCategoryDto;
import com.example.notefraisapigateaway.tool.RestClient;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {
    public ResponseCategoryDto create(RequestCategoryDto dto){
        RestClient<ResponseCategoryDto,RequestCategoryDto> restClient = new RestClient<>("");
        return restClient.post(restClient.getServers().get(1),"api/category",dto, ResponseCategoryDto.class);
    }
    public ResponseCategoryDto findById(int id){
        RestClient<ResponseCategoryDto,String> restClient = new RestClient<>("");
        return restClient.get(restClient.getServers().get(1),"api/category/"+id, ResponseCategoryDto.class);
    }
    public String delete(int id){
        RestClient<String,String> restClient = new RestClient<>("");
        return restClient.deleteById(restClient.getServers().get(1),"api/category",String.class,id);
    }
    public ResponseCategoryDto[] findAll(){
        RestClient<ResponseCategoryDto[],String> restClient = new RestClient<>("");
        return restClient.get(restClient.getServers().get(1),"api/category",ResponseCategoryDto[].class);
    }
}
