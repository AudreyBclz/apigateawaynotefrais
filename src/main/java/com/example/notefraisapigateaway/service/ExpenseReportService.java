package com.example.notefraisapigateaway.service;

import com.example.librairienotefrais.dto.RequestExpenseReportDto;
import com.example.librairienotefrais.dto.ResponseExpenseReportDto;
import com.example.notefraisapigateaway.tool.RestClient;
import com.example.notefraisapigateaway.tool.RestClientUpload;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

@Service
public class ExpenseReportService {

    public ResponseExpenseReportDto createExpenseReport(RequestExpenseReportDto dto){
        RestClient<ResponseExpenseReportDto,RequestExpenseReportDto> restClient = new RestClient<>("");
        return restClient.post(restClient.getServers().get(1),"api/expense-report",dto,ResponseExpenseReportDto.class);
    }
    public ResponseExpenseReportDto uploadAndCreationProof(MultipartFile file, int id)throws IOException {
        RestClientUpload<ResponseExpenseReportDto,MultipartFile>restClient = new RestClientUpload<>();
        return restClient.post("http://localhost:8082","api/expense-report/upload/"+id,file, ResponseExpenseReportDto.class);
    }
    public ResponseExpenseReportDto[]findBySomeId(int id,String searched){
        RestClient<ResponseExpenseReportDto[],String> restClient = new RestClient<>("");
        return  restClient.getSomeById(restClient.getServers().get(1),"api/expense-report"+searched+"/",ResponseExpenseReportDto[].class,id);
    }
    public ResponseExpenseReportDto[] findAll(){
        RestClient<ResponseExpenseReportDto[],String> restClient = new RestClient<>("");
        return restClient.get(restClient.getServers().get(1),"api/expense-report",ResponseExpenseReportDto[].class);
    }
    public ResponseExpenseReportDto findById(int id){
        RestClient<ResponseExpenseReportDto,String> restClient = new RestClient<>("");
        return restClient.get(restClient.getServers().get(1),"api/expense-report/"+id, ResponseExpenseReportDto.class);
    }
    public ResponseExpenseReportDto updateStatus(RequestExpenseReportDto dto,int id){
        RestClient<ResponseExpenseReportDto,RequestExpenseReportDto> restClient = new RestClient<>("");
        return restClient.patch(restClient.getServers().get(1),"api/expense-report",dto, ResponseExpenseReportDto.class,id);
    }
}
