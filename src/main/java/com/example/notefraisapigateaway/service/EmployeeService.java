package com.example.notefraisapigateaway.service;

import com.example.librairienotefrais.dto.RequestEmployeeDto;
import com.example.librairienotefrais.dto.ResponseEmployeeDto;
import com.example.notefraisapigateaway.tool.RestClient;
import org.springframework.stereotype.Service;

@Service
public class EmployeeService {


    public ResponseEmployeeDto create(RequestEmployeeDto dto){
        RestClient<ResponseEmployeeDto,RequestEmployeeDto> restClient = new RestClient<>("");
        return restClient.post(restClient.getServers().get(0),"api/employee",dto,ResponseEmployeeDto.class);
    }
    public ResponseEmployeeDto[] findAll(){
        RestClient<ResponseEmployeeDto[],String> restClient = new RestClient<>("");
        return restClient.get(restClient.getServers().get(0),"api/employee",ResponseEmployeeDto[].class);
    }
    public ResponseEmployeeDto findById(int id){
        RestClient<ResponseEmployeeDto,String> restClient = new RestClient<>("");
        return  restClient.get(restClient.getServers().get(0),"api/employee/"+id, ResponseEmployeeDto.class);
    }
    public ResponseEmployeeDto delete(int id){
        RestClient<ResponseEmployeeDto,String> restClient = new RestClient<>("");
        return restClient.deleteById(restClient.getServers().get(0),"api/employee", ResponseEmployeeDto.class,id);
    }
    public ResponseEmployeeDto findBySearch(String search){
        RestClient<ResponseEmployeeDto,String> restClient = new RestClient<>("");
        return restClient.get(restClient.getServers().get(0),"api/employee/login/"+search, ResponseEmployeeDto.class);
    }
}
