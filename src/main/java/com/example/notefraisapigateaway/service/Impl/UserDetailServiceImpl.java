// Partie pour Spring Security
package com.example.notefraisapigateaway.service.Impl;

import com.example.notefraisapigateaway.entity.Employee;
import com.example.notefraisapigateaway.entity.UserDetailImpl;
import com.example.notefraisapigateaway.service.EmployeeService;
import com.example.notefraisapigateaway.tool.DtoEmployee;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailServiceImpl implements UserDetailsService {
    @Autowired
    private EmployeeService employeeService;

    @Autowired
    private DtoEmployee _dtoUtils;

    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException{
        Employee employee = _dtoUtils.convertToEntity(employeeService.findBySearch(username));
        return UserDetailImpl.build(employee);
    }
}
